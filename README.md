ROnDir (Rust [Ondir][ondir])
=====

Introduction
------------
rondir is a fork of [ondir] rewritten in rust.
It is a small program to automate tasks specific to certain directories.
It works by executing scripts in directories when you enter and leave them.

[Run command (rc) scripts][rc] show how to automate this when using either
 bash, fish, tcsh, or straight up sh.

Getting Started
---------------
1. Add the [script relevant to your shell][rc] to your rc-script.
2. Restart your shell.
3. Add an entry to your ~/.rondirrc such as those described below. 
4. Change into the corresponding path.
5. Check for success.

Details
-------
An example of rondirs usefulness is when editing web pages. I have a umask of 0777
by default, but when creating web pages in ~/public_html the web content has
to be readable by the user the web server runs as. By adding a path section for
this directory to my ~/.rondirrc, and corresponding enter and leave sub-sections,
any scripts in the enter/leave sub-sections are executed when I enter and leave
the directory, respectively. Here is how the entry in my ~/.rondirrc would look::

```
enter /home/athomas/public_html
    umask 022

leave /home/athomas/public_html
    umask 077
```

And that's all it does. Simple, but effective. 

rondir takes one parameter, the directory you are leaving.

Note that these scripts will be executed when you pass THROUGH the directory 
as well. Using the preceding example, typing "cd ~/public_html/mywebpage" will 
execute the 'enter' in ~/public_html. The reverse is also true: when leaving 
a path, all 'leave' scripts in the intermediate directories are executed.

A more useful example
---------------------
Ondir is particularly useful with `virtualenv
<http://pypi.python.org/pypi/virtualenv>`_. The following config will
automatically activate virtualenv's when you change into a directory and
deactivate when you move out::

  enter ~/Projects/([^/]+)
    if [ -r .venv ]; then
      . ./.venv/bin/activate
    fi

  leave ~/Projects/([^/]+)
    deactivate > /dev/null 2>&1

Similar programs
----------------

 - [direnv] - Like ondir, but for inject environment variables into your shell 
              after executing a script
 - [go ondir] - go rewrite of ondir
 - [ondir] - The original from which this is a fork of


[direnv]: https://direnv.net/
[go ondir]: https://github.com/glejeune/ondir
[ondir]: https://github.com/alecthomas/ondir
[rc]: src/resources/rc
