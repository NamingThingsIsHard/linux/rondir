use clap::CommandFactory;
use clap_mangen::Man;
use rondir_cli::Cli;

fn main() -> std::io::Result<()> {
    let cmd = Cli::command();

    let man = Man::new(cmd);
    let mut buffer: Vec<u8> = Default::default();
    man.render(&mut buffer)?;

    std::fs::write("rondir.1", buffer)?;
    Ok(())
}
