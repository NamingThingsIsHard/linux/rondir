use std::path::Path;

use crate::config::{Action, Config, ConfigEntry};

/// Finds a ConfigEntry within the config that matches the given path change
///
/// Might turn up empty if the path didn't change or no matching ConfigEntry can be found
pub fn get_matching_entry(src: &Path, dest: &Path, config: Config) -> Option<ConfigEntry> {
    let src_str = src.to_str()?;
    let dest_str = dest.to_str()?;
    for entry in config.entries {
        for rule in entry.clone().rules {
            let src_matches = rule.is_match(src_str);
            let dest_matches = rule.is_match(dest_str);

            match determine_action(src_matches, dest_matches) {
                Some(action_taken) if action_taken == entry.action => return Some(entry),
                _ => continue,
            }
        }
    }
    None
}

/// Determine the action from the directory movement
///
/// # Examples
///
/// ```
/// use crate::logic::determine_action
/// use crate::config::Action
///
/// // Coming from /
/// let src = "/";
/// // Entering /opt/projects/
/// let dest = "/opt/projects";
///
/// // A rule exists for movements entering or exiting /opt/projects
/// let rule = "/opt/projects/";
///
/// // Rules does NOT match
/// let src_matches = src == rule;
/// // Then it does
/// let dest_matches = dest == rule;
///
/// // The expected direction is "entering /opt/projects"
/// assert!(determine_action(src_matches, dest_matches), Some(Action::Entry))
/// ```
///
/// See [`crate::config::Action`]
pub fn determine_action(src_matches: bool, dest_matches: bool) -> Option<Action> {
    match (src_matches, dest_matches) {
        (false, true) => Some(Action::Entry),
        (true, false) => Some(Action::Exit),
        (_, _) => None,
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use std::path::PathBuf;

    use regex::Regex;

    use crate::config::{Action, Config, ConfigEntry};
    use crate::logic::get_matching_entry;

    fn assert_entry(entry: Option<ConfigEntry>) {
        assert_eq!(entry.clone().unwrap().action, Action::Entry)
    }
    fn assert_exit(entry: &Option<ConfigEntry>) {
        assert_eq!(entry.clone().unwrap().action, Action::Exit)
    }
    fn assert_no_config(entry: Option<ConfigEntry>) {
        assert!(entry.is_none())
    }
    fn mk_config() -> Config {
        Config {
            location: Default::default(),
            entries: vec![
                ConfigEntry {
                    rules: vec![Regex::new("/mnt/special/").unwrap()],
                    script: "echo I entered!".to_string(),
                    action: Action::Entry,
                },
                ConfigEntry {
                    rules: vec![Regex::new("/mnt/special/").unwrap()],
                    script: "echo I exited!".to_string(),
                    action: Action::Exit,
                },
            ],
        }
    }

    #[test]
    fn test_get_matching_entry__enter_exact() {
        assert_entry(get_matching_entry(
            &PathBuf::from("/mnt/"),
            &PathBuf::from("/mnt/special/"),
            mk_config(),
        ));
    }
    #[test]
    fn test_get_matching_entry__enter_child() {
        assert_entry(get_matching_entry(
            &PathBuf::from("/mnt/"),
            &PathBuf::from("/mnt/special/whatever/"),
            mk_config(),
        ));
    }
    #[test]
    fn test_get_matching_entry__enter_wrong_folder() {
        assert_no_config(get_matching_entry(
            &PathBuf::from("/mnt/"),
            &PathBuf::from("/etc/system.d/"),
            mk_config(),
        ));
    }
    #[test]
    fn test_get_matching_entry__exit_exact() {
        assert_exit(&get_matching_entry(
            &PathBuf::from("/mnt/special/"),
            &PathBuf::from("/etc/system.d/"),
            mk_config(),
        ));
    }
    #[test]
    fn test_get_matching_entry__same_root() {
        assert_no_config(get_matching_entry(
            &PathBuf::from("/mnt/special/some/folder"),
            &PathBuf::from("/mnt/special/another/folder"),
            mk_config(),
        ));
    }
}
