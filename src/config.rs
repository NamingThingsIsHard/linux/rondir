use std::fs::read_to_string;
use std::iter::zip;
use std::path::PathBuf;

use anyhow::{anyhow, Context, Result};
use regex::Regex;

/// Can also be thought of the direction of movement into or out of a directory
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Action {
    /// A movement into the directory
    /// e.g `cd ~/project` from the working directory /tmp/ means the entry of `~/project`
    Entry,
    /// A movement out of the directory
    /// e.g `cd /tmp` from the working directory ~/project/rondir/ means the exit of `~/project/rondir`
    Exit,
}

/// A model for the configuration stored in a file
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Config {
    pub location: PathBuf,
    pub entries: Vec<ConfigEntry>,
}
/// A section in the configuration that allows matching a directory to a rule
///
/// If a rule matches, that means the script it contains can be used
#[derive(Clone, Debug)]
pub struct ConfigEntry {
    pub rules: Vec<Regex>,
    /// What should be executed / returned should a rule match
    pub script: String,
    /// What is allowed to trigger this entry
    pub action: Action,
}

impl PartialEq for ConfigEntry {
    fn eq(&self, other: &Self) -> bool {
        if self.rules.len() != other.rules.len() {
            return false;
        }
        for (left, right) in zip(self.rules.clone(), other.rules.clone()) {
            if left.as_str() != right.as_str() {
                return false;
            }
        }
        self.action == other.action
    }
}

impl Eq for ConfigEntry {}

/// Uses the builder pattern to make a ConfigEntry
#[derive(Default)]
pub struct ConfigEntryBuilder {
    rules: Vec<Regex>,
    /// A list of lines as there might be some string processing later
    script_lines: Vec<String>,
    action: Option<Action>,
}
impl ConfigEntryBuilder {
    /// A string is expected as it's assumed the regex will be from a line in a text file
    ///
    /// TODO Support `~` in path
    pub fn add_rule(&mut self, regex_str: &str) -> Result<()> {
        self.rules
            .push(Regex::new(regex_str).context(format!("Invalid regex {}", regex_str))?);
        Ok(())
    }
    pub fn add_script_line(&mut self, line: &str) {
        self.script_lines.push(line.to_string())
    }
    pub fn set_action(&mut self, action: Action) {
        self.action.replace(action);
    }
    pub fn has_rules(&self) -> bool {
        !self.rules.is_empty()
    }

    pub fn build(&mut self) -> Result<ConfigEntry> {
        if self.rules.is_empty() {
            Err(anyhow!("Cannot create ConfigEntry without rules"))
        } else if self.script_lines.is_empty() {
            Err(anyhow!("Cannot create ConfigEntry without script"))
        } else if self.action.is_none() {
            Err(anyhow!("Cannot create ConfigEntry without action"))
        } else {
            Ok(ConfigEntry {
                rules: self.rules.clone(),
                script: self.script_lines.join("\n"),
                action: self.action.as_ref().unwrap().clone(),
            })
        }
        // No implicit reset here as a new ConfigEntry might be made from the same data
    }

    /// Prepare to create another ConfigEntry
    pub fn reset(&mut self) {
        self.rules.clear();
        self.action.take();
        self.script_lines.clear();
    }
}

/// Read a config object from a file
/// It has the format of
///
/// (<action> /<regex>)+
/// <script line>+
///
/// Where:
/// <action> = enter|exit
pub fn read_config(path: PathBuf) -> Result<Config> {
    Ok(Config {
        location: path.clone(),
        entries: parse_config_entries(&read_to_string(path)?)?,
    })
}

fn parse_config_entries(content: &str) -> Result<Vec<ConfigEntry>> {
    let mut entries: Vec<ConfigEntry> = vec![];
    let rule_regex = Regex::new(r"^(?<action>enter|exit)\s+(?<regex>/.*)")?;
    let mut entry_builder: ConfigEntryBuilder = Default::default();

    for line in content.lines() {
        // Found a rule
        if let Some(captures) = rule_regex.captures(line) {
            // Got a new regex and could create a new entry
            // Commit it and start creating a new entry
            if let Ok(config_entry) = entry_builder.build() {
                entries.push(config_entry);
                entry_builder.reset();
            }

            // Either entry was created and we start a new one
            // or rules a still being added to the existing entry being build
            entry_builder.set_action(
                match captures
                    .name("action")
                    .ok_or(anyhow!("Couldn't get get an action from the rule"))?
                    .as_str()
                {
                    "enter" => Action::Entry,
                    "exit" => Action::Exit,
                    _ => return Err(anyhow!("Unknown action")),
                },
            );
            entry_builder.add_rule(
                captures
                    .name("regex")
                    .ok_or(anyhow!("Couldn't get get a regex from the rule"))?
                    .as_str(),
            )?;
        }
        // Handle script after rules have been collected
        else if entry_builder.has_rules() {
            entry_builder.add_script_line(line.trim());
        }
    }
    // Might've read the last script line and are able to create a new config entry
    if let Ok(config_entry) = entry_builder.build() {
        entries.push(config_entry);
    }
    Ok(entries)
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use regex::Regex;

    use crate::config::{parse_config_entries, Action, ConfigEntry};

    #[test]
    /// Only one rule is in the file
    fn single_rule() {
        let entries = parse_config_entries(
            r#"
enter /mnt/special/
    echo I entered special!
    echo Second line"#,
        )
        .unwrap();
        assert_eq!(entries.len(), 1);
        let entry = entries.get(0).unwrap();
        assert_eq!(
            entry,
            &ConfigEntry {
                rules: vec![Regex::new("/mnt/special/").unwrap()],
                script: r#"echo I entered special!
echo Second line"#
                    .to_string(),
                action: Action::Entry
            }
        );
    }
    #[test]
    /// Multiple rules for one entry
    fn multiple_rules() {
        let entries = parse_config_entries(
            r#"
enter /etc/nix/
enter /mnt/special/
    echo I entered a directory!!
    echo Second line"#,
        )
        .unwrap();
        assert_eq!(entries.len(), 1);
        let entry = entries.get(0).unwrap();
        assert_eq!(
            entry,
            &ConfigEntry {
                rules: vec![
                    Regex::new("/etc/nix/").unwrap(),
                    Regex::new("/mnt/special/").unwrap()
                ],
                script: r#"echo I entered a direcotry!!
echo Second line"#
                    .to_string(),
                action: Action::Entry
            }
        );
    }
    #[test]
    /// Multiple entries with one having multiple rules
    fn multiple_entries() {
        let entries = parse_config_entries(
            r#"
enter /etc/nix/
enter /mnt/special/
    echo I entered a directory!!
    echo Second line
enter /home/user/projects/nix
    echo entered nix
exit /home/user/projects/nix
    echo exited nix
            "#,
        )
        .unwrap();

        assert_eq!(
            entries,
            [
                ConfigEntry {
                    rules: vec![
                        Regex::new("/etc/nix/").unwrap(),
                        Regex::new("/mnt/special/").unwrap()
                    ],
                    script: r#"echo I entered a direcotry!!
echo Second line"#
                        .to_string(),
                    action: Action::Entry
                },
                ConfigEntry {
                    rules: vec![Regex::new("/home/user/projects/nix").unwrap()],
                    script: "entered nix".to_string(),
                    action: Action::Entry
                },
                ConfigEntry {
                    rules: vec![Regex::new("/home/user/projects/nix").unwrap()],
                    script: "exited nix".to_string(),
                    action: Action::Exit
                }
            ]
        );
    }
    #[test]
    /// An entry composed only of rules
    /// Won't work because a script is needed to make an entry complete
    fn incomplete_entry__only_rules() {
        let entries = parse_config_entries(
            r#"
        enter /etc/nix/
        enter /mnt/special/"#,
        )
        .unwrap();
        assert_eq!(entries.len(), 0);
    }
    #[test]
    /// An entry composed only of rules
    /// Won't work because a script is needed to make an entry complete
    fn incomplete_entry__only_script() {
        let entries = parse_config_entries("echo I just entered the domain").unwrap();
        assert_eq!(entries.len(), 0);
    }
    #[test]
    /// An entry where there action isn't the first thing on the line should fail
    fn action_not_first_on_line() {
        let entries = parse_config_entries(
            r#"
            enter /mnt/special
                echo rule doesn't start with action. Uses spaces instead"#,
        )
        .unwrap();
        assert_eq!(entries.len(), 0);
    }
}
