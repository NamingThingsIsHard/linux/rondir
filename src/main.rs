mod config;
mod logic;
use expanduser::expanduser;
use std::env::current_dir;
use std::path::PathBuf;

use anyhow::{anyhow, Context, Result};
use clap::Parser;

use rondir_cli::Cli;

fn main() -> Result<()> {
    let cli = Cli::parse();

    let src: PathBuf = cli.src;

    // Default destination is the CWD
    let dest_opt: Option<PathBuf> = cli.dest;
    let dest = dest_opt
        .context("No destination provided")
        .or_else(|_err| current_dir().context("Couldn't fall back to current directory"))?;

    // Ensure paths are absolute
    if !(src.is_absolute() && dest.is_absolute()) {
        return Err(anyhow!("Source and destination have to be absolute paths"));
    }
    // TODO support configuration of the config file location
    let config = config::read_config(expanduser("~/.ondirrc")?)
        .context("Couldn't read the configuration")?;
    if let Some(config_entry) = logic::get_matching_entry(&src, &dest, config) {
        println!("{}", config_entry.script)
    }
    Ok(())
}
