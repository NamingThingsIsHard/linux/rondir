# Developing

As this is a Rust rewrite, you need to install [rust][rust install].

You can then

```shell
# Install clippy and rustfmt
rustup component add clippy
rustup component add rustfmt

# Format code
cargo fmt
# Lint code
cargo clippy --package rondir --package rondir_cli -- -D warnings

# Build
cargo build

# Run the debug version
./target/debug/rondir --help

# Build release (optimized for size)
cargo build --release

# Run release version
./target/release/rondir --help
```

# Commits

Try to stick to [conventional commits] and prefer to [rebase] to get rid of
 commits like "did X", "forgot Y", "undid Y", "oops", "woops", etc.


[conventional commits]: https://www.conventionalcommits.org/en/v1.0.0/
[rebase]: https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase
[rust install]: https://www.rust-lang.org/tools/install
