use clap::Parser;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[arg(help = "The previous directory")]
    pub src: PathBuf,
    #[arg(help = "The target directory. Defaults to the current directory")]
    pub dest: Option<PathBuf>,
}
